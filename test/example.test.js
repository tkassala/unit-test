// example.test.js
const expect = require('chai').expect;
const assert = require('chai').assert;
const mylib = require('../src/mylib');

describe('Unit testing math.js', () => {

    absTestNumbers = [];

    before(() => {
        absTestNumbers = [-100,-50,0,50,100];
        console.log(`Set abs test numbers to ${absTestNumbers}`);
        console.log('Performing tests...');
    })

    it('Should always return non-negative values', () => {
        let results = [];
        absTestNumbers.forEach(n => {
            results.push(mylib.absolute(n));
        });

        assert(!results.some(r => r < 0));

    })

    it('Should return string "OK"', () => {
        expect(mylib.OK).to.equal("OK");
    })

    after(() => {console.log('All tests done!')});

})
